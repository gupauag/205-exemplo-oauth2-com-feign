package br.com.mastertech.lanchonete.lanchonete;

import br.com.mastertech.lanchonete.lanches.Lanche;
import br.com.mastertech.lanchonete.lanches.LancheClient;
import br.com.mastertech.lanchonete.security.principal.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LanchoneteController {

    @Autowired
    private LancheClient lancheClient;

    @GetMapping
    public Lanchonete getLanchonete(@AuthenticationPrincipal Usuario usuario) {
        Lanchonete lanchonete = new Lanchonete();
        lanchonete.setDono(usuario.getName());
        lanchonete.setLanches(lancheClient.getLanches());

        return lanchonete;
    }



    /*
    * Exemplo de endpoint que o usuário não precisa estar logado para acessar,
    * Mas o Feign acessará um endpoint que utiliza OAuth2, ou seja
    * essa api vai precisar fazer o login de client_credentials para acessar esse recurso do Feign
    * */
    @GetMapping("/teste")
    public List<Lanche> getLanches() {
        return lancheClient.getLanches();
    }

}

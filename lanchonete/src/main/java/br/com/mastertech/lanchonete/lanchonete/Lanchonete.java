package br.com.mastertech.lanchonete.lanchonete;

import br.com.mastertech.lanchonete.lanches.Lanche;

import java.util.List;

public class Lanchonete {

    private String dono;

    private List<Lanche> lanches;

    public String getDono() {
        return dono;
    }

    public void setDono(String dono) {
        this.dono = dono;
    }

    public List<Lanche> getLanches() {
        return lanches;
    }

    public void setLanches(List<Lanche> lanches) {
        this.lanches = lanches;
    }
}

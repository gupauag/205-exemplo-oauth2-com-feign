package br.com.mastertech.lanche;

import br.com.mastertech.lanche.security.principal.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class LancheController {

    @GetMapping
    public List<Lanche> getLanche(@AuthenticationPrincipal Usuario usuario) {
        Lanche lanche = new Lanche();
        lanche.setTipo("X-Salada");
        lanche.setDono(usuario.getName());

        List<Lanche> lanches = new ArrayList<>();
        lanches.add(lanche);
        return lanches;
    }
}
